# Report for Creative Commons

### Sam Muirhead, Mozilla Fellow, hosted Sep '18 - Jun'19 by Creative Commons

## The short version:

I summarized the key themes and outputs of my work in a 3-minute presentation given at Ford Foundation in NYC in May:

[![](./title.png)](https://vimeo.com/sammuirhead/drawing-on-the-commons)

[**Drawing on The Commons: Digital Co-Creation for Activism**](https://vimeo.com/sammuirhead/drawing-on-the-commons)

## The long version:

### Background
I've been investigating the potential of commons-based arts projects for a long time, and I've been particularly fascinated by the idea of 'open sourcing' animation projects by sharing their project files under CC licenses - allowing different people around the world to remix them and adapt them to their own purpose and context. I think this approach would be particularly useful for the fields of [activism, education, & civic organisations](https://vimeo.com/206399471), and I've seen the beneficial effects of this practice from my own experience sharing modular design files with other chapters of [distributed networks](https://oscedays.org).

I have developed 'remixable' animation & media projects in the past, using the existing tools of artistic media creation (desktop animation, audio, and video editing programs) and combining them with the existing tools of 'open source' online collaboration, which are designed with software development in mind (git repositories). 

**Example:** [Frag den Staat animation project](https://gitlab.com/cameralibre/FragDenStaat-Animation).

This had very limited success, as potential participants/collaborators needed to be familiar with _both_ of these very complex and different workflows... this sets an extremely high barrier to entry. Even stepping back from animation to focus on illustration - a simpler form of visual storytelling - proved overly complex.

### Approach for the fellowship

So it was clear to me that for remixable visual storytelling to be successful, _I could not rely on the existing tools_. It would need a new, more streamlined approach, which combines a simplified artistic media workflow, with a simplified online collaboration workflow - only replicating or adapting the most necessary parts of each. 

An answer lies in the multipurpose and ubiquitous tool on every smartphone and computer: the web browser. Building a browser-based workflow would enable the creation, discussion, adaptation, history tracking and publishing of visual storytelling projects to happen without having to jump from one tool to another. Ideally, when somebody _views_ an openly licensed vector graphic illustration on the web, the ability to _remix_ that image should only be a click away.

The Scalable Vector Graphics (SVG) image format is also an important part of the puzzle. This is a native graphics format of the web, which is an open standard and can be read and written by all web browsers, all code editors, and all digital illustration & design programs (Inkscape, Adobe Illustrator, Sketch, Figma, and many more). It can thus serve as a bridge between the different workflows of a very wide range of participants. When considering 'remixability', SVG is special in that the file _is_ its 'source code' - it is similar to an HTML page, with different elements written as `<circle>`, `<line>` and so on. An SVG is a _recipe_ for a browser to display an image - so it is extremely easy to edit that recipe and swap out different 'ingredients' to achieve a different effect. 
Just like HTML web pages, SVG images can also be animated, made interactive, and store other files and data within them - so the spectrum of potential future uses for an online SVG remixer are huge.

### Upskilling

One small snag is that this online SVG remixer would need to be programmed. I was not a software developer, and I did not have budget to commission one. I would need an effective proof-of-concept to be able to demonstrate the potential of this idea, and I would need to learn to code in order to get there.
This is an unusual activity for a Mozilla Fellow to undertake - normally Fellows either _already_ code in the course of their daily work, or it is not a skill which is necessary or a priority for them. Over the course of my research in these past years, my lack of coding ability has blocked me from taking this idea further, and one of the aims of this fellowship is to set us up for future sucess in our fields - this had to be a priority for me. So in the first few months, while other Fellows were covening policy meetings with members of the European Parliament or writing articles for major publications, I was busy writing 'for loops' on FreeCodeCamp.com, and reading books about the intricate inner workings of SVG files.

By April I was able to write a simple online program for remixing SVG templates - a CC heart design combined with different national flags can be further remized for any future or existing CC chapter to use on their online or printed materials, or turned into a button or sticker. 
This functionality is not the end point of the project, but rather the beginning - a more in-depth explanation of this can be found in my developer diary: '[_It's not about the buttons_](https://gitlab.com/cameralibre/cc-buttons/wikis/Dev-Diary-2019-04-25#its-not-about-the-buttons)', but the general idea is that I'm working towards an SVG remixing tool which can remix _any_ vector illustration (not just templates), which provides commenting/discussion functionality, and which shows some kind of version history and 'forking' of images. 
For the remainder of my fellowship I am prioritizing the re-writing of the tool with these ends in mind.

### Themes: artistic expression, autonomy, solidarity, self-representation & empowerment

Another area that was vital for me to learn more about were issues of inequality, colonization and [representation](https://gitlab.com/cameralibre/mozilla-fellowship/blob/master/Representation-and-Diversity-in-Illustration.md) in the fields of technology and design, and working out what I could do avoid perpetuating historical inequalities, and to support horizontal activism rather than top-down hierarchical processes. 
What I'm interested in is enabling people to express their own perspectives, in a way that feels culturally appropropriate, and for the collaboration on remixable imagery to be a many-to-many collaboration, rather than a campaign which is designed in an organisation's HQ in the global north, before then being 'tweaked' slightly to fit the rest of the world. This meant a lot of reading (see my [Reading List](https://gitlab.com/cameralibre/mozilla-fellowship/issues/1) for an idea), and this education will continue long after the fellowship. 

The final area of focus for me was more social - spreading commons approaches in artistic circles, and helping people who might not identify as 'creative' to experiment with and feel more confident with artistic expression and collaboration. This included many different activities - at Open Source Design Summit in Tirana, Albania, I talked with designers new to CC licensing about their concerns and skepticism regarding online collaboration & free culture, and in Wellington, helped local illustrators understand how to work within the NZ Government's open licensing requirements. 

I ran Cut, Copy & Paste remix workshops at Mozilla Festival, Wellington ZineFest, Open Source Design Summit, the Mozilla Fellows Winter Summit, HBK Saar (a visual arts university in Saarbrücken, Germany), and the CC Global Summit. 

I also developed a new workshop format for the CC Summit which is designed to help activists to express their goals visually, and be able to use imagery to articulate the better future they're working towards. This workshop will be further refined in July, when I run it for a coalition of child welfare activist groups here in Wellington - it will then be documented and open sourced in the same way that I have open sourced [Cut, Copy & Paste](https://gitlab.com/cameralibre/cut-copy-and-paste), enabling other facilitators to adapt and run the workshop in other parts of the world. 

Related to 'spreading commons approaches in artistic circles' was my work on 'connecting existing artists of the commons'. Here, my main activity has been proposing and organising a new collaboration platform for artists`* (in a broad sense of that word) who are using CC licenses or otherwise working in the spirit of the commons. This platform is called [CC-Create](https://discourse-dev.labs.creativecommons.org), and it is currently running as a two-month experiment - we're planning a retrospective in July to talk about what is working, what is not, and what we might want to do differently in the future.  
The aim of the platform is for CC artists to find collaborators, to exchange stories and ideas about business models, tools and resources, and share creative projects published under open licenses. The reason to set up an _asynchronous_ discussion forum rather than rely solely on a _synchronous_ chat channel is, first, an acknowledgement of the small size of the existing CC artist community, and the sparse and thin connections that exist between us. With the current numbers of artists on the CC Slack channels, we cannot sustain lively multi-participant conversations, whereas with the slower _asynchronous_ approach, we can build up valuable resources and discussions over a period of weeks, rather than minutes.

**Example:** [Daisy-Chain Interview Thread](https://discourse-dev.labs.creativecommons.org/t/daisy-chain-interview-thread/51)

This approach also means that older discussions can be easily found and referenced, meaning that common knowledge can be built up across the network rather than relying on a few key people as knowledge 'bottlenecks'.


### My experience with Creative Commons as a community & host organization

I have been using and advocating for Free Culture and Creative Commons licensing for many years. I got to know many people in the CC community through MozFest 2017 & the CC Summit 2018, and got started with CC Create shortly afterward. The goals of Creative Commons are very much aligned with my own and I've found many friends and collaborators in the network - I was already very involved with my host organisation before the fellowship, and I'm very happy that I will continue to be in the future. 

My regular bi-weekly calls with Majd Al-shihabi & Claudio Ruiz were the best part of my experience with Creative Commons - an opportunity to check-in, ask questions, and chat in an informal setting. Even though Majd & I were working on quite different projects, I learned a huge amount from him and his experiences as a Bassel Khartabil Fellow!

Overall, however, I felt that the level of inclusion with Creative Commons was relatively low - compared to other fellows' experiences with their hosts, or my own expereince with Mozilla Foundation, where I was added to staff email lists, invited to take part in regular calls, All Hands meetings etc. This experience with Mozilla made it much easier to understand how things worked with the Foundation, what they struggled with, how they worked together. 

Naturally, it's easier to accommodate one more person in a larger organisation like Mozilla. But I had a lot more difficulty with Creative Commons: everybody was very helpful and willing to help me or provide information when I asked, but I had to know _to ask_. 
I was invited to present on a staff call this month, and after the presentaion I really enjoyed listening in on the rest of conversation, as it gave me a better understanding of who does what, what the key issues were for CC at the moment, what projects were underway, and where I could possibly be useful... If Creative Commons were to host more Fellows in the future I would **highly** recommend including them on more staff calls - not necessarily to present their work, but simply to listen.

It has also been wonderful to see the work of the CC Tech team over the last year in their efforts to become more open to participation, more transparent, and to document the state of ongoing and past projects - as somebody struggling to understand the inner workings of the CC staff and network, and as a beginner coder, I found their work extremely helpful and welcoming. 

I struggled with the relatively short timespan of the fellowship, particularly when working remotely - it was sometimes hard to see opportunities to contribute, or to build relationships, and I know that would have been easier with more time. I don't feel that I have been as _useful_ to Creative Commons as I would have liked - but this is also due to the nature of my process for the fellowship, which was heavily focused on learning. I'm very excited about collaborating on projects like CC Search & CC Vocabulary in the future, but felt that my knowledge & my work was not up to a useful level until very recently. Similarly, I'm very keen to work with different network chapters on testing my ideas, but my online tool wasn't ready to go during the fellowship so other than asking questions and interviewing people, I couldn't really engage much early on.

This has not been a 'side project' for me, but rather part of a long-term focus on arts, activism and the commons - so my fellowship activities (and my collaborations with Creative Commons) will continue long after July 1st - albeit at a lower intensity.

## Next steps:

### SVG remix tool: 

- I'm currently working together with Mix Irving to finish a re-write of the tool. Key [milestones](https://gitlab.com/cameralibre/svg-jam/-/boards) are that it should work with _any_ SVG image, that people can write, view, and reply to comments, & that new images/components can be inserted). 
- The tool will then be tested with a local activist coalition, and with the CC Uruguay team.

### Workshops

- I'm planning a 'Visualizing your vision / Translating images' workshop with a Wellington-based coalition of activists in July, which will help refine the workshop and allow me to start documenting it for others to use and adapt.
- In the next 2 months I'm running a 1-hour web-based workshop about Scalable Vector Graphics, adapting a collaborative design workshop for the local design university, and have two more Cut Copy & Paste workshops lined up.
- A idea that I would like to pursue later this year would be running an online version of the 'visualizing your vision' workshop with CC network members.

### CC Create

- In July I'll hold a retrospective with participants of the first two months of CC-Create. This will be both in written form and in video calls, to look back at first experiments, and decide what the next steps for the platform should be, and how we might work together in the future.
- One activity which I'll personally be advocating for is setting up monthly calls, for us to be able to get to know each other better and maintain a good working rhythm.
