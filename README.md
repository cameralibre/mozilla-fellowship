# Open Source Visual Storytelling for Activism
### an Open Web Fellowship project
Hi! I'm Sam, and from September 2018-June 2019, I'm working with the international [Creative Commons](https://creativecommons.org/) network as a [Ford-Mozilla Open Web Fellow](https://advocacy.mozilla.org/en-US/open-web-fellows/overview). 

I'm working on tools, methods and online spaces for open artistic collaboration on visual storytelling (that is: illustration, design, animation, comics) which can support activist campaigns. 

Essentially, where diverse groups of activists are working on similar goals, but in different contexts, I believe it would be beneficial to pool time and resources to develop visual storytelling elements together, and adapt that imagery to fit their specific local contexts. This is possible when works are easily 'forked' for different purposes, and where multiple versions of an idea can exist and evolve in parallel. 
In this way solidarity is built across networks, fewer resources are wasted than if everyone were redundantly replicating each others' work, and higher quality, more specific, more relevant and more surprising communication can be developed.

I have previously been exploring these concepts in my [Cut, Copy & Paste](https://gitlab.com/cameralibre/cut-copy-and-paste) workshops, through [animation for activism](https://vimeo.com/206399471), and through co-founding a global network and event series advocating for an [Open Source Circular Economy](https://oscedays.org/).

## Vision/Mission Statement

I want to help citizens, artists and activists explore their creativity through collaboration, and use that creativity to further the causes that are important to them. The commons and the open source methodology is the basis of my practice - all the work that I do should encourage others to use, share and build upon it.

Through my work, I aim to:

- celebrate and support a multiplicity of different perspectives, _'towards a world where many worlds fit'_
- help activists build solidarity beyond their cultural context, and be more effective within it.
- give artists an opportunity to use online collaboration to work on meaningful, surprising and inspiring projects


## What is the problem that you’re trying to solve?

	 	 	 	
- Campaigns for Internet Health issues [like copyright](https://creativecommons.org/2018/09/12/with-the-european-parliament-vote-on-the-copyright-directive-the-internet-lost-for-now/), as well as broader social and environmental justice campaigns, are up against powerful, well-funded interests. We need to be as effective as possible with the resources available to us. That means getting more creative minds involved, working in collaboration rather than isolation, and being specific and inspiring in our communication.

- Visual storytelling (and the internet) tends to look the same everywhere! Text is often translated for different audiences, but too often imagery is not - instead it is stripped of specifics to become one-size-fits-all, in the process being stripped of much of its potency and cultural relevance.

- Designers, illustrators, comics artists and animators are currently using their creative energy and experience on predominantly commercial projects. Work on more meaningful projects (such as for activist groups or non-profits) is often seen as a chaotic collaboration process and not financially viable. If funding could be pooled across multiple aligned activist groups, if the collaboration process could be made more enjoyable, and if the results were more inspired, more surprising, and more effective, I believe that more artists would engage with meaningful work.


## Deliverables:

(to be completed by the end of my fellowship - July 2019)

### Visual Storytelling Campaign
	 	 	 	
A co-created, 	'translated' and remixed visual storytelling campaign developed with Creative Commons network chapters (illustration, 	comics, graphic design, animation), with a focus on creation in / adaptation to local cultural contexts, encouraging the use of diverse visual languages and culturally appropriate expression. This may take the form of social media images, animated gifs, printed zines, comics or posters.

### P2P Web App
A peer-to-peer web application for viewing and adapting illustrations & comics, which can be accessed using [Beaker Browser](https://beakerbrowser.com/) (Beaker is free / libre / open source, and as easy to use as a regular web browser.). This template can be used by local Creative Commons chapters as the primary tool for forking and adapting imagery. This web application will comprise:

- a simple visual SVG editor
- the ability for users to easily make a version-controlled copy (this is a basic function of Beaker)
- the ability to easily add / replace SVG elements
- a method of comparing (**diff**ing) individual images
- a method of visualising the ‘evolution’ of a work over time (tree view)
- instant publication on the p2p internet (automatic with Beaker)
- export to other formats (eg. png,  pdf )
- publication to the web (eg CC-Create forum, social media).

### CC-Create network platform
The [CC-Create network platform](https://github.com/creativecommons/network-platforms/blob/master/Culture.md) will be a forum for discussion and collaboration on creative projects. It will also serve as workspace for the Creative Commons network to continue collaborating on these visual storytelling projects and build bridges between artists and activists.

### Open Educational Resources
Documented workshop format / tutorial / open curriculum for other people to take this process into art schools, events etc. 
