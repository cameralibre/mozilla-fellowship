# Writing on Representation & Diversity in Illustration
### from kind of non-diverse sources...

_**note:** these are all written in English, from a largely North American or European persepctive - if you know of any, I would love to read articles in other languages/from other contexts! Similarly, the western graphic design and illustration world is overwhelmingly white, so other voices aren't as common/publicized. 
Any pointers to more articles writtern by BIPoC authors would be greatly appreciated. 
For some reason the majority of these articles come from Silicon Valley software companies - do you know of more people (like Una) writing from an activism perspective?_

Comments and additions welcome!
If you're short on time, my top 3 articles in this list are probably those from Zara Rahman, Una Lee and Jennifer Horn, but they all have something to offer.

----

![](https://www.andalsotoo.net/wp-content/uploads/2017/03/IMG_2708-2200x1100.jpg)

- **The Vision Archive, 2015: _[Collaborative Aspirational Iconography](https://www.andalsotoo.net/stories/collaborative-aspirational-iconography/)_** - Una Lee
  - a project to collaboratively create new imagery, by and for activists
  - What if social movement imagery were visionary in addition to being critical? 
  - And what if it were created by organizers and community members to reflect the world they are working towards?
  
----

![](https://ph-files.imgix.net/28b297ac-38d5-4478-b758-b37416b10038)

- **Slack, Jan 2015: _[Just a Brown Hand](https://medium.com/@uxdiogenes/just-a-brown-hand-313db35230c5)_** - Diógenes Brito
  - an example of using a brown skin-tone emoji in Slack's communication, and how it felt for the designer and audience
  - ''Why was the choice an important one, and why did it matter to the people of color who saw it? The simple answer is that they rarely see something like that. These people saw the image and immediately noticed how unusual it was.'  

----
![](https://uploads-ssl.webflow.com/5bc8ec0a2f93635b4cb3d3cb/5bf7046e4fac80813c6b098b_wp_char_04%402x-p-1600.png)
- **Wordpress, May 2017: _[Inclusiveness in illustration](https://www.byalicelee.com/wordpress)_** - Alice Lee
  - depicting your typical illustrated "latte / laptop world" comes off as very exclusionary to a huge part of the audience that don't exist in this flashy sphere — mom-and-pop shops, individual entrepreneurs, small / medium businesses, etc:

----
![](https://cdn-images-1.medium.com/max/1600/1*2LHUdc80Cm_AZ2IfLranuA.png)
- **Atlassian, Jan 2018: _[Illustrating Balanced and Inclusive Teams](https://medium.com/designing-atlassian/illustrating-balanced-and-inclusive-teams-c548166c7540)_** - Sara VanSlyke

  - a wide range of specific characters to be re-used throughout Atlassian branding
  - color-coding makes it easier for any team member to include diverse illustrations in their slide decks

![](https://cdn-images-1.medium.com/max/2600/1*9YTwtT1aHKDqq83DqyBzrw.png)

- **Shopify, Jan 2018: _[You Can't Just Draw Purple People and Call it Diversity](https://ux.shopify.com/you-cant-just-draw-purple-people-and-call-it-diversity-e2aa30f0c0e8)_** - Meg Robichaud
  - We can’t exclude anyone if we’ve kinda included no one, right?
  - It’s easy to draw the developer as a woman and pat yourself on the back- _see what we did there? women can computer too, you know_. [...] it’s a good place to start, not a good place to stop. 

----
![](https://www.dailydot.com/wp-content/uploads/2018/11/skin-tone-emoji.jpg)

- **The Daily Dot, Nov 2018: _[The problem with emoji skin tones that no one talks about](https://www.dailydot.com/irl/skin-tone-emoji/)_** - Zara Rahman
  -  There are those for whom the choice over how to identify, especially when it comes to “color,” has been a source of conflict in real life, not just when sending a text.
  -   In describing its skin tone changes, the Unicode Consortium released a technical report where it stated:
_“General-purpose emoji for people and body parts should also not be given overly specific images: the general recommendation is to be as neutral as possible regarding race, ethnicity, and gender.”_ This wording and desire are fascinating. Because of the way that the skin tones were added, as “modifiers” to the “general-purpose” emoji, there arose a need for a default, a color to which the skin tones would be added. But who is this “neutral” person they speak of? What is a neutral ethnicity, or a neutral race or gender?

----
![](https://cdn-images-1.medium.com/max/2400/1*_S7MhRD-KxvCmfho0_0aDA.png)

- **AirBnB, Dec 2018: _[Your Face Here](https://medium.com/airbnb-design/your-face-here-9aa1d4970d6c)_** - Jennifer Horn
  - “When a person appears as an outlined white space while their hair and clothing have color, it’s easy to assume they’re caucasian.”
  - By referencing real photos of our hosts and guests, historical figures, friends, families, and coworkers, we’re able to represent the people in our product with as much diversity as we see in the world.

----
![](https://cdn-images-1.medium.com/max/2400/1*cJ4x67S4BXZIb9IA1ovwNg.png)

- **Facebook, Mar 2019: _[Illustrating a More Inclusive Brand](https://medium.com/elegant-tools/illustrating-a-more-inclusive-brand-bbb4fa6c4bb3)_** - Trace Byrd
  - Being inclusive means being global:  By placing an emphasis on Western-style clothing, architecture, and businesses, we are alienating a huge portion of the people who use Facebook.

----

![](https://i0.wp.com/www.nativeamericacalling.com/wp-content/uploads/2019/03/Graphic-Design-PHOTO-tatankawitko.jpg)

- **Native America Calling, March 2019: _[A Better Image of America through Graphic Design](https://www.nativeamericacalling.com/tuesday-march-19-2019-a-better-image-of-native-america-through-graphic-design)_ (Audio)** - Tara Gatewood
  - Interview with Neebinnaukzhik Southall, Sadie Red Wing and Ryan Red Corn about indigenous approaches to graphic design. Deals with issues such as providing positive images of Indigenous culture to counteract negative stereotypes.

----
