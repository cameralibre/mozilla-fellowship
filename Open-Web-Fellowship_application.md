>I'm going to be working with Creative Commons as a [Ford-Mozilla Open Web Fellow](https://advocacy.mozilla.org/en-US/open-web-fellows/overview)! 🎉🌈💥✨

>When I was applying for the Open Web Fellowship, there was a really useful ['Ask Me Anything' session](https://public.etherpad-mozilla.org/p/2018-fellowship-ama) with Aurelia Moser from the Mozilla Fellowships team. In it, she linked to a number of successful Open Science Fellow applications for us to learn from, but there weren't any published applications from Open Web Fellows - perhaps because Open Web Fellowships are in very specific contexts at different organisations, I don't know. But in the interests of transparency, and hopefully to be of help to future applicants, here's my application. 
(Also because I haven't written a project README yet, so this works as a placeholder!)

>The only organisation I was applying to was Creative Commons, so their [blog post](https://creativecommons.org/2018/03/21/cc-will-host-2018-19-ford-mozilla-open-web-fellow/) gives some context for my application.

--------------------------
  **Project proposal: What is project you would like to pursue during your fellowship?**
  
  *Please include: 1) What is the issue(s) that your project will address? 2) What is the tangible product or set of products you will aim to create during your fellowship? 3) What is the broader impact you want that project to have in the world? 4) How will your specific project advance the long arc of the topic you’re working on?*
  
--------------------------
  
International advocacy networks like Creative Commons can struggle to campaign as effectively as adversaries with deeper pockets, or more political power at their disposal. Even with many people around the world working on the same cause, it’s hard to make their perspectives and ideas visible and understandable to a broad audience.

This is where visual storytelling comes in.

Illustration, infographics, graphic design, comics and animation. All extremely powerful tools to communicate complex topics in an identifiable way, with emotion, with humor, or with stark clarity.

Currently, local chapters of a network can lack the resources for high-quality creative work, or can waste time tackling the same problems as their peers. A central HQ could contract a creative agency to communicate for the network with a One-Size-Fits-All approach, and translate text for different local chapters. But why just the text?
Why can’t we easily ‘translate’ images to be more appropriate or meaningful to a local context?
Why can’t we empower diverse chapters and artists to co-create and adapt imagery, both to explore universal concepts and tell local visual stories?

The current models to create visual storytelling aren’t using the full potential of the open web. With scalable vector graphics, version control, responsive design, interactivity and more at our disposal, why can’t visual storytelling on the web be as flexible, as creative and as empowering as open source software?

Through research and collaboration, I will prototype open visual storytelling in support of Creative Commons, as an open source proof-of-concept which can then be adapted by other international advocacy networks.

**Output 1: Localised open source graphics, illustrations & animations** for campaigns, co-created with Creative Commons' diverse local groups for use across web, print & more, at any scale. Not generic icons or clip art, but rather tailor-made, beautiful illustration.
This content will be adaptable to different formats: an illustration can be turned into a comic, then an animation…
More importantly, these graphics will be adaptable to local context: I’ve already prototyped this, making an animation for a German Freedom of Information website, and adapting it for the Hungarian equivalent: translating text, switching color palettes, adapting the look of postboxes, government buildings, and other elements to fit a Hungarian context. Then documenting the process so that others can adapt the animation to other FOI websites.

Open source graphics shouldn’t entail just the adaptation of imagery but also its co-creation. Based on my experiments with Cut, Copy & Paste workshops (where participants enthusiastically fork & remix each other’s work) I see huge potential here in the simpler end of the visual storytelling spectrum: illustration, graphic design, and comics. However, this needs some structure to work properly:

**Output 2: A collaborative workflow, and technical tools** to allow local groups to engage in the co-creation of these graphics, edit, discuss drafts, and easily adapt them to their own local context & culture, language, and color palette.

Many of these tools already exist as open source programs, but need streamlining into a workflow appropriate for a wide range of participants. The aim is to encourage a diverse range of people to be more involved in the creation of visual culture, and invite illustrators and designers into a new, exciting collaborative process.


 
--------------------------
  **What do you consider to be the biggest threat to the open web in your area of expertise? Why? In what way would the Internet -- or the world -- be different if this threat were mitigated?**
  
--------------------------
  
The book that really got me started down this path was Yochai Benkler’s _The Wealth of Networks_. 
It opens with a story about the development of radio: initially, a p2p technology where each device would send and receive... but worn down through corporate interests and legislation, it became the centralized, one-way broadcast system that dominates today - in which citizens only receive, but have no power to send.

The moral of Benkler’s story being that the beneficial peer production processes which exist today can only exist on an open, neutral,peer-to-peer network. This idea has been foremost in my mind ever since.

Corporate projects that selectively provide web access (like Facebook’s Internet Basics) or legislation to end net neutrality (as in Portugal & the US) severely harm the internet’s basic function, and its creative development. They seek to turn the most empowering human and technological network that we’ve ever developed... into cable television with Likes.

We need to bring more artists and more diversity into internet culture, right now, to be more efficient and effective in education and communicating these threats to the wider world. 

Creative campaigning can bring more benefits than just 'stopping bad stuff happening': for artists, open visual storytelling could allow them to work how they want, where they want, without having to flock to major cities for commercial work – it could offer paid work on beneficial projects rather than just advertising, and it creates truly fertile works which can live on in new forms. It opens up a new values system in which building on the work of others is something valuable and creative, rather than merely a ‘derivative work’...

Defending the internet doesn't mean taking a break from improving it. How about we work on both mitigating the threat, and building the positive future at the same time?
 
--------------------------
  
 **Why are you the right candidate for 10 months of self-directed study? Describe your current professional arrangement.**

--------------------------
  
I'm an extremely motivated and self-directed learner, a documentation addict, and have done in-depth research projects in this field before eg. Year of Open Source, or Cut, Copy & Paste. But I've never been funded to work on a project before. I've been able to do quite a lot so far in my spare time, but it's not sustainable - a Fellowship would allow me to be a lot more effective by focusing entirely on one project.

I work freelance as an animator and videographer, and I try to align my paid work & my open source contributions with my long-term goals - for example, I'm currently making modular, open source animations explaining concepts of the decentralized Secure Scuttlebutt network - these can be used by anyone in the community in blog posts, documentation, talks, documentaries etc - but they can also be adapted by other decentralized projects which need to explain similar concepts.


 
--------------------------
  
 
  **What are two projects (with URLs) that you are proud of? What did you do on them and why are you proud?**
  
--------------------------
  
**[Year of Open Source](http://edition.cnn.com/2013/06/04/opinion/sam-muirhead-year-open-source)**

For a year, I stopped buying All Rights Reserved/patented products, and instead sought out or developed open source solutions in every aspect of my life, to communicate this strange nerdy concept in an accessible way: by researching, interviewing hardware hackers, making videos, and then developing my own projects. Then publishing articles & videos about making digitally knitted clothing, my own open design furniture, parametric boxer shorts, DIY soap, 'Free' beer, my CC music collection, etc.

I'm proud that, through interviews & press articles about the project, open source **beyond software** was finally discussed in mainstream channels, and now even my Dad understands what open source is!

**[Open Source Circular Economy Days](https://oscedays.org)**

When starting OSCEdays in 2014, we were advocating collaboration on a global scale, from grassroots to government, to corporations... But we were a team of 6 people, with no funding.

The solution was to create toolkits and invite local groups to organize their own meetings as part of a global event. I set up infrastructure, wrote articles & tutorials, gave presentations, and worked on community building. We provided our local groups with modular design kits, a discussion forum, slide decks and texts to adapt, and connected them within the network.

The result was magic.

Within 6 months, we launched a networked event in 32 cities on 5 continents, with over a 1000 participants worldwide: from the government in Shenzhen, to the Philips HQ in Eindhoven, to an outdoor FabLab in Côte d'Ivoire. The next year we repeated it in over 100 cities, and OSCEdays continues to grow.

I’m proud of how much we achieved with the few resources we had, and that 4 years later, the project lives on without relying on its founders, with an established rhythm of events, and local chapters that are active year-round.

  
  
 
--------------------------
  
 
  **Is there anything else you’d like to share with us about yourself regarding the Fellowship?**

--------------------------

After 8 years in Berlin, in the center of Europe's open web & commons movement, 3 months ago I moved back home to Aotearoa New Zealand, to be closer to family. My challenge now is to find a way in which I can still be strongly connected to the international people and movements which I find so important, despite being down the end of a long internet cable at the bottom of the world ;) 

I believe that the Open Web Fellowship and a connection to the international Creative Commons network can provide a rare opportunity to be able to work on my creative and political goals, while maintaining a base in Aotearoa, so I'm excited to have the chance to apply. 
  
--------------------------
  
--------------------------
 


***ADDITIONAL QUESTIONS FOR OPEN WEB FELLOWS***
 
--------------------------
 
--------------------------
   **Which host organizations are most interesting to you (select up to 2)?**
  
--------------------------
  Creative Commons
 
  
--------------------------
   **Why do you want to be a fellow at the organization or organizations you selected? How do you see this collaboration contributing to your career development?**
   
--------------------------

My individual long-term goals of a vibrant, collaborative artistic commons are very well aligned with those of Creative Commons. Specifically within the scope of a 10-month Fellowship, CC's goal of conducting effective campaigns across a diverse and distributed affiliate network is a perfect fit with:

**1)** my experience doing the same with OSCEdays, and

**2)** my creative goal to facilitate the development of adaptable, open source graphics that can be easily adapted to different purposes and cultural contexts and serve the needs of activist groups.

I would like to be working with (or at least accountable to) a team, and apply the community-building skills that I learned with OSCEdays to an organization aligned with my particular focus: the intersection of open source and culture.

I have been wanting to get stuck into this project full-time for more than a year, but have been lacking both the time to truly dedicate myself to it, and the right community to perform experiments on ;)

In terms of career development, what I'm looking for is being able to work consistently towards my goals on a focused project, without having to hustle for freelance gigs on the side to pay the bills. My previous projects have been undertaken without any institutional backing, but I would love to have the support of this network to exchange ideas, get support where I need it, and find connections to the right people at the right time.

I met many members of the CC staff and network at this year's Summit and was blown away by how welcomed I felt, and all the kind, intelligent people who make up the community. At the same time, I was struck by how much work in the network was focused on _releasing existing knowledge_ under open licenses, compared to the amount of discussion on my focus: **_collaborative creation of new culture_**. So I feel that I could fill an important gap at CC in that sense. I'd love to be able to support an organization that has essentially made the work that I do possible.



  
--------------------------
   **Why is it important for civil society to actively promote or incorporate open web practices?**\


--------------------------
Activists and non-profits, wherever they are in the world, share challenges with others working in the same way, or on the same issues. Many of these challenges have either been successfully tackled before, and should be learned from, or they could be more effectively tackled by pooling time and resources across organizations, on a shared project. In the past this was technically, socially and organizationally difficult. But now, the open web movement offers tools, learning resources and years of experience to make this process vastly easier.

Additionally, the decentralized and diverse nature of community organizing in different contexts should be an asset to civil society, but it doesn't align well with centralized, top-down structures of big NGOs. In order to bring in a wide array of ideas and perspectives, and make the most of the individuals within a movement, a more participatory working method is needed, with roles and pathways for people of different backgrounds and motivations.
  
--------------------------
  
 **How do you want your work to be sustained after the fellowship?**\*

--------------------------
Goals:

- leave a functional infrastructure and clear tutorials & documentation for others to follow

- work openly, inviting others into the development of the project to bring in new perspectives and increase its resilience.

- separate to the fellowship, I'm applying to become a CC member, and getting involved in setting up a 'Culture' platform for the network, so I will continue to be a part of the network long after a fellowship has finished.

I love it when somebody else has left me a delicious trail of breadcrumbs to follow, so I always make documentation & tutorials a priority for my projects: see [_'Why Should I Document My Challenge?'_](http://community.oscedays.org/t/why-should-i-document-my-challenge/4935)
 
--------------------------
   **How does your work demonstrate an interest in working open? List any experiences in developing open source/access projects.**
   
--------------------------
  
I spent a year applying the concept of open source to every aspect of my life, and collaborating with open designers and hardware hackers on projects like open source underwear and furniture (Year of Open Source).

I'm active in the Open Source Design community, and I'm a user and community member (donations, bug reports, advocacy, forum posts) of various creative Free Software programs, including Kdenlive, Inkscape, & Synfig.

I run post-production workshops with F/LOSS and create open learning resources for video.

All of my work is released under open licenses.

I often work with open organizations like Open Knowledge, Wikimedia & OuiShare.

I co-founded Open Source Circular Economy Days, developing infrastructure and community building strategy (adaptable resources, modular design kits, how-to-guides).

I have developed and led hands-on workshops introducing open collaboration to non-coders, and I participated in Mozilla Open Leaders program as a project leader, and now mentor.