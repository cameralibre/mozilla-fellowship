var shapes = document.getElementsByClassName("svg-shape")

for (var i = 0; i < shapes.length; i++) {
    shapes[i].addEventListener('click', pickerClicker);
}

function pickerClicker(e) {
    var pickerId = (this.id + "Picker");
    document.getElementById(pickerId).focus();
    document.getElementById(pickerId).click();
}

document.addEventListener("input", changeShapeColor)

function changeShapeColor (e) {

  var inputName = e.target.name
  var newColor = document.getElementsByName(inputName)[0].value
  console.log({newColor})


  var elId = inputName.replace('input','').replace('Color','')

  console.log({elId})

  document.getElementById(elId).style.setProperty("--fill-color", newColor)
}
