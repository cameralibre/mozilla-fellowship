// first, the rgb version:

const gradientsCircle = document.getElementById("rgb-gradients-circle")

const shapes = document.getElementsByClassName("svg-shape")

for (let i = 0; i < shapes.length; i++) {
    shapes[i].addEventListener('click', pickerClicker);
}

function pickerClicker(e) {
    let pickerId = (e.target.id + "-picker");
    document.getElementById(pickerId).focus();
    document.getElementById(pickerId).click();
}


document.addEventListener("input", checkHexOrHsl)


function checkHexOrHsl(e) {
  let inputName = e.target.name
  let newColor = document.getElementsByName(inputName)[0].value
  let elId = inputName.replace('input-','').replace('-color','')

  if (inputName == 'input-rgb-flat-color') {
    changeShapeColorRgb(elId, newColor)
  } else {
    changeShapeColorHsl(elId, newColor)
   }
}

function changeShapeColorRgb (elId, newColor) {
  document.getElementById(elId).style.setProperty("--rgb-fill-color", newColor)
  let hex = newColor.replace('#','')
  console.log(hex)

  hexToRGB(hex)
  function hexToRGB(hex) {
    let decR = parseInt(hex.slice(0, 2), 16),
        decG = parseInt(hex.slice(2, 4), 16),
        decB = parseInt(hex.slice(4, 6), 16);
    let outerRadialStop = [(decR - 22), (decG + 24), (decB - 52)]
    let innerRadialStop = [(decR - 22), (decG + 44), (decB - 35)]
    let linearStop0 = [(decR + 22), (decG + 24), (decB + 33)]
    let linearStop1 = [(decR + 22), (decG - 10), (decB + 16)]

    const stops = [outerRadialStop, innerRadialStop, linearStop0, linearStop1]


    rgbToHex(stops)
    function rgbToHex(stops) {
      for (let i = 0; i < stops.length; i++) {
        let stop = stops[i]

        for (let j = 0; j < stop.length; j++) {

          let lowerLimit = Math.max(0, stop[j])
          let upperLimit = Math.min(255, lowerLimit)
          let hexValue = parseInt(upperLimit, 10).toString(16);

          let formattedHex = ("0" + hexValue).slice(-2);
          stop[j] = formattedHex
        }

        let hexcode = ("#" + stop[0] + stop[1] + stop[2])
        stops[i] = (hexcode)

        gradientsCircle.style.setProperty("--rgb-outer-radial-stop", stops[0])
        gradientsCircle.style.setProperty("--rgb-inner-radial-stop", stops[1])
        gradientsCircle.style.setProperty("--rgb-linear-stop-0", stops[2])
        gradientsCircle.style.setProperty("--rgb-linear-stop-1", stops[3])
      }
    }
  }
}

// and now for HSL:

let hslGradientsCircle = document.getElementById("hsl-gradients-circle")

function changeShapeColorHsl(elId, newColor) {
  let hexcode = newColor.replace('#','')
  console.log(hexcode)
  hexToHsl(hexcode)

  function hexToHsl(hexcode) {
    var r = parseInt(hexcode.slice(0, 2), 16),
        g = parseInt(hexcode.slice(2, 4), 16),
        b = parseInt(hexcode.slice(4, 6), 16);
    return rgbToHsl(r, g, b)
  }

  function rgbToHsl(r, g, b){
    r = +r/255, g = +g/255, b = +b/255;
    var max = Math.max(r, g, b), min = Math.min(r, g, b);
    var h, s, l = (max + min) / 2;
    if(max == min){
        h = s = 0; // achromatic
    }else{
        var d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
        switch(max){
            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
            case g: h = (b - r) / d + 2; break;
            case b: h = (r - g) / d + 4; break;
        }
        h /= 6;
    }
    h = Math.floor(h * 360)
    s = Math.floor(s * 100)
    l = Math.floor(l * 100)
    setColors(h, s, l);
    return(h, s, l);
  }

  function setColors(h, s, l) {
    let innerRadialStop = []
    let linearStop1 = []

    let hslFillColor = ("hsl(" + h + ", " + s + "%, " + l + "%)")
    document.getElementById(elId).style.setProperty("--hsl-fill-color", hslFillColor)

    let outerRadialStop = [(h + 27 % 360), (Math.max(s, 21) - 21), (Math.max(l, 21) - 14)]

    if (h < 7) {
      innerRadialStop = [(h + 352), (Math.max(0,(s - 18))), (Math.max(0, (l - 11)))]
    } else {
      innerRadialStop = [(h - 8), (Math.max(s, 18) - 18), (Math.max(l, 11) - 11)]
    }
    debugger
    let linearStop0 = [(h + 3 % 360), (Math.min(s, 81) + 19), (Math.min(l, 91) + 9)]

    if (h < 13) {
      linearStop1 = [(h + 346), (Math.min(s, 81) + 19), (Math.min(l, 94) + 6)]
    } else {
      linearStop1 = [(h - 14), (Math.min(s, 81) + 19), (Math.min(l, 94) + 6)]
    }

    let stops = [outerRadialStop, innerRadialStop, linearStop0, linearStop1]
    console.log(stops)

    setGradients(stops)
    function setGradients(stops) {
      for (var i = 0; i < stops.length; i++) {
        var stop = stops[i]
        var hslColor = ("hsl(" + stop[0] + ", " + stop[1] + "%, " + stop[2] + "%)")
        stops[i] = (hslColor)
      }
      hslGradientsCircle.style.setProperty("--hsl-outer-radial-stop", stops[0])
      hslGradientsCircle.style.setProperty("--hsl-inner-radial-stop", stops[1])
      hslGradientsCircle.style.setProperty("--hsl-linear-stop-0", stops[2])
      hslGradientsCircle.style.setProperty("--hsl-linear-stop-1", stops[3])
      debugger
    }
  }
}
