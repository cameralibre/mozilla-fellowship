var isSetup = false
var isEditable = false

document.getElementById("editability").addEventListener('click', function(e) {
  if (e.target.checked) {
    isEditable = true
    document.getElementById("inputs").style.display = "contents"

    if (isEditable && !isSetup) {
      setupListeners()
      isSetup = true
    }

    function setupListeners () {
      var shapes = document.getElementsByClassName("svg-shape")

      for (var i = 0; i < shapes.length; i++) {
        shapes[i].addEventListener('click', pickerClicker);
      }
      isSetup = true
    }

    document.addEventListener("input", changeShapeColor)
  }

  else {
    isEditable = false
    isSetup = false
    document.getElementById("inputs").style.display = "none"
    var shapes = document.getElementsByClassName("svg-shape")

    for (var i = 0; i < shapes.length; i++) {
      shapes[i].removeEventListener('click', pickerClicker);
    }
  }
})

function pickerClicker (e) {
  var pickerId = (e.target.id + "Picker")
  document.getElementById(pickerId).focus()
  document.getElementById(pickerId).click()
}

function changeShapeColor (e) {
  var inputName = e.target.name
  var newColor = document.getElementsByName(inputName)[0].value
  var elementId = inputName.replace('input','').replace('Color','')
  document.getElementById(elementId).style.setProperty("--fill-color", newColor)
}
