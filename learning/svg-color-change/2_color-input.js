document.addEventListener("input", changeBackgroundColor)

function changeBackgroundColor (e) {

  var inputName = e.target.name
  //when a click event occurs, this will store the name of the thing that was clicked

  var newColor = document.getElementsByName(inputName)[0].value
  // there should only be one item in the array 'inputName', so we can safely choose the first item [0], and get its value (the color that was picked)

  var elId = inputName.replace('input','').replace('Color','')
  //this strips out the beginning and end of the input name, leaving us with only the id of the shape

  document.getElementById(elId).style.setProperty("--fill-color", newColor)
  // set the CSS variable '--fill-color' on that element to the value of newColor
}

//
// function condensedChangeBackgroundColor (e) {
//   var inputName = e.target.name
//   var newColor = document.getElementsByName(inputName)[0].value
//   var elId = inputName.replace(/(input|color)/gi, '').toLowerCase()
//
//   document.getElementById(elId).style.setProperty("--background-color", newColor)
// }
