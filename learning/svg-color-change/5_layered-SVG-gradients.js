var shapes = document.getElementsByClassName("svg-shape")
var gradientsCircle = document.getElementById("gradients-circle")


for (var i = 0; i < shapes.length; i++) {
    shapes[i].addEventListener('click', pickerClicker);
}

function pickerClicker(e) {
    var pickerId = (e.target.id + "-picker");
    document.getElementById(pickerId).focus();
    document.getElementById(pickerId).click();
}

document.addEventListener("input", changeShapeColor)

function changeShapeColor (e) {

  var inputName = e.target.name

  var newColor = document.getElementsByName(inputName)[0].value
  var elId = inputName.replace('input-','').replace('-color','')

  document.getElementById(elId).style.setProperty("--fill-color", newColor)
  var hex = newColor.replace('#','')
  console.log(hex)

  hexToRGB(hex)
  function hexToRGB(hex) {
    var decR = parseInt(hex.slice(0, 2), 16),
        decG = parseInt(hex.slice(2, 4), 16),
        decB = parseInt(hex.slice(4, 6), 16);
    console.log(decR, decG, decB)
    var outerRadialStop = [(decR - 22), (decG + 24), (decB - 52)]
    var innerRadialStop = [(decR - 22), (decG + 44), (decB - 35)]
    var linearStop0 = [(decR + 22), (decG + 24), (decB + 33)]
    var linearStop1 = [(decR + 22), (decG - 10), (decB + 16)]


    var stops = [outerRadialStop, innerRadialStop, linearStop0, linearStop1]


    rgbToHex(stops)
    function rgbToHex(stops) {
      for (var i = 0; i < stops.length; i++) {
        var stop = stops[i]

        for (var j = 0; j < stop.length; j++) {

          var lowerLimit = Math.max(0, stop[j])
          var upperLimit = Math.min(255, lowerLimit)
          var hexValue = parseInt(upperLimit, 10).toString(16);

          var formattedHex = ("0" + hexValue).slice(-2);
          stop[j] = formattedHex
        }

        var hexcode = ("#" + stop[0] + stop[1] + stop[2])
        stops[i] = (hexcode)

        gradientsCircle.style.setProperty("--outer-radial-stop", stops[0])
        gradientsCircle.style.setProperty("--inner-radial-stop", stops[1])
        gradientsCircle.style.setProperty("--linear-stop-0", stops[2])
        gradientsCircle.style.setProperty("--linear-stop-1", stops[3])
      }
    }
  }
}
