let fileName = ""
let mySVG = {}

function displaySVG() {
  let file = document.getElementById('upload').files[0]
  fileName = document.getElementById('upload').files[0].name
  let text = new Response(file).text()

  text.then(function(result){
    svgData = result
    let display = document.getElementById("image")
    display.innerHTML = result
    let displayedSVG = document.getElementsByTagName('svg')[0]
    mySVG = SVG.adopt(displayedSVG)
  })
}

function blueifySVG() {

  let rects = SVG.select('rect').members
  let circles = SVG.select('circle').members
  let polygons = SVG.select('polygon').members
  let paths = SVG.select('path').members
  let polylines = SVG.select('polyline').members
  let texts = SVG.select('text').members
  let shapes = [].concat(rects, circles, polygons, paths, polylines, texts)
  console.log(shapes)

  // convert other color formats to hsl
  for (i=0; i<shapes.length; i++) {

    let shape = shapes[i]
    let color = shape.attr('fill')

    // convert hexcodes (''#ffffff')
    let hexcode = color.match(/#[a-fA-F0-9]{6}/g)

    if (hexcode !== null) {
      let hsl = hexToHsl(hexcode)
      shape.attr('fill', hsl)
    }

    // convert hex shortcodes ('#fff')
    let shortcode = color.match(/#[a-fA-F0-9]{3}$/g)
    if (shortcode !== null) {

      let r = shortcode[0].charAt(1)
      let b = shortcode[0].charAt(2)
      let g = shortcode[0].charAt(3)

      let hexcode = ["#"+r+r+b+b+g+g]
      let hsl = hexToHsl(hexcode)
      shape.attr('fill', hsl)
    }


    // convert rgba or rgb values ('rgba(255, 255, 255, 100)')
    let rgbColor = color.match(/rgba?\([\d\s]+, [\d\s]+, [\d\s]+(, [\d\s]+)?\)/)
    if (rgbColor !== null) {
      let rgbArray = rgbColor[0].match(/\((.+)\)/)[1].split(',')
      var r = rgbArray[0],
          g = rgbArray[1],
          b = rgbArray[2];
      let hsl = rgbToHsl(r, g, b)
      shape.attr('fill', hsl)

    }

    // convert web color names
    color = color.toLowerCase()
    if (Object.keys(webColors).includes(color)) {
      shape.attr('fill', webColors[color])
    }

    // find target hsl color and convert to blue
    let target = shape.attr('fill')

    if (target == "hsl(0, 100%, 50%)") {
      shape.attr('fill', "hsl(240, 100%, 50%)")
    }
  }
}

function downloadSVG() {
  let base64Data = btoa(mySVG.svg())
  let dataURL = "data:text/html;charset=utf-8;base64," + base64Data
  let blueFileName = fileName.replace(".svg", "") + "-blue.svg"
  let link = document.getElementById('download-link')
  link.href = dataURL
  link.download = blueFileName
  link.click()
}

function applyFilter(){
	const filterValues = ['normal', 'grayscale', 'protanopia', 'deuteranopia', 'deuteranomaly', 'tritanopia', 'tritanomaly', 'achromatomaly']
	var filterSelector = document.getElementById("filter-selector")
	let whichFilter = filterSelector.selectedIndex
  if (filterValues[whichFilter] == 'normal'){
    mySVG.attr('filter', null)
  }
  else {
  	let selectedFilter = ('url(#' + filterValues[whichFilter] + ')')
    mySVG.attr('filter', selectedFilter)
  }
}

// hexToHsl(hexcode)
function hexToHsl(hexcode) {
  var r = parseInt(hexcode[0].slice(1, 3), 16),
      g = parseInt(hexcode[0].slice(3, 5), 16),
      b = parseInt(hexcode[0].slice(5, 7), 16);

  return rgbToHsl(r, g, b)
}

function rgbToHsl(r, g, b){
  r = +r/255, g = +g/255, b = +b/255;

  var max = Math.max(r, g, b), min = Math.min(r, g, b);
  var h, s, l = (max + min) / 2;



  if(max == min){
      h = s = 0; // achromatic
  }else{
      var d = max - min;
      s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
      switch(max){
          case r: h = (g - b) / d + (g < b ? 6 : 0); break;
          case g: h = (b - r) / d + 2; break;
          case b: h = (r - g) / d + 4; break;
      }
      h /= 6;
  }
  let result = "hsl(" + Math.floor(h * 360) + ", " + Math.floor(s * 100) + "%, " +  Math.floor(l * 100) + "%)"
  return result;
}
