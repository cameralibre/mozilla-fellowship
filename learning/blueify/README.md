# Blueify
_The incredible, award-winning, beautifully-designed web application. Now taking seed funding._

Oh, the things it can do!

- Import & display an SVG file
- Convert the fill colors of all rectangles to HSL format
- Find any fill colors which are red, and convert them to blue
- Download the modified SVG file
- that's pretty much it