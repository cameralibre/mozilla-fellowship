## Imagemagick 'compare'

[imagemagick.org/script/compare](https://www.imagemagick.org/script/compare.php)

> Use the ```compare``` program to mathematically and visually annotate the difference between an image and its reconstruction. 

Works on raster images, so requires conversion of SVG before diffing

## GSVG - Git-friendly SVG

[github.com/noahbrenner/gsvg](https://github.com/noahbrenner/gsvg) - _last commit Feb 2017_

> Reformat SVG files to reduce ```git diff``` noise

The goal of GSVG is to create a consistent, deterministic formatting for SVG files. This is valuable no matter what SVG editor you use. The formatting that GSVG creates is modeled after Inkscape's output, it just fixes some inconsistencies.

## SVGO & SVGOMG

[github.com/svg/svgo](https://github.com/svg/svgo) - _last commit Oct 2018_

> SVG Optimizer is a Nodejs-based tool for optimizing SVG vector graphics files.

This is intended for filesize optimization rather than normalization of an SVG's structure. But maybe still useful..?

- Sara Soueidan's [overview of SVGO tools](https://www.sarasoueidan.com/blog/svgo-tools/)
- Jake Archibald's [SVGOMG](https://jakearchibald.github.io/svgomg/) - 'SVGO's Missing GUI'

![](https://d33wubrfki0l68.cloudfront.net/1889124c6bd60abf9e4f6a3c736b386ded44c436/1d291/images/svgomg.png)

## diffsvg

[github.com/jrsmith3/diffsvg](https://github.com/jrsmith3/diffsvg) - _last commit Jul 2013_

A basic test of whether an element's id exists in **A.svg** & **B.svg**, with changes pretty-printed to stdout.


## Kactus for Sketch

[kactus.io](https://kactus.io/)

![](https://user-images.githubusercontent.com/3254314/27239695-4615f8f2-52d2-11e7-99df-48867813a49c.png)

## Github/GitLab image viewing & diffing
[help.github.com/articles/rendering-and-diffing-images//](https://help.github.com/articles/rendering-and-diffing-images/) 


### 2-up
![](https://help.github.com/assets/images/help/repository/images-2up-view.png)

### Swipe
![](https://help.github.com/assets/images/help/repository/images-swipe-view.png)

### Onion Skin
![](https://help.github.com/assets/images/help/repository/images-onion-view.gif)

## Design with git

[github.com/xuv/design-with-git](https://github.com/xuv/design-with-git) - _last commit Jul 2013_

In-depth write-up: [w.xuv.be/projects/design_with_git](http://w.xuv.be/projects/design_with_git)

Working example with multiple view methods: [xuv.github.io/design-with-git](http://xuv.github.io/design-with-git/)

![](design-with-git.png)




